package main

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"os"
	"strings"
)

// Search items in a specified S3 Bucket
//
// Usage:
// go run main.go PROFILE REGION BUCKET_NAME KEYWORD
func main() {
	if len(os.Args) != 5 {
		exitErrorf("Profile, Region, Bucket name, and Keyword required\nUsage: %s profile region bucket_name keyword",
			os.Args[0])
	}

	profile := os.Args[1]
	region := os.Args[2]
	bucket := os.Args[3]
	keyword := os.Args[4]

	// Specify profile to load for the session's config
	sess  := getSession(profile, aws.String(region))

	// Create S3 service client
	svc := createS3Service(sess)

	// Get the list of items
	resp := getObjects(svc, aws.String(bucket))

	fmt.Printf("There are %d items found for keyword '%s'", search(resp, keyword), keyword)
}

func exitErrorf(msg string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, msg+"\n", args...)
	os.Exit(1)
}

func search(resp *s3.ListObjectsOutput, keyword string) int {
	counter := 0
	for _, item := range resp.Contents {
		if strings.Contains(strings.ToUpper(*item.Key), strings.ToUpper(keyword)) {
			counter++
			fmt.Println("Name:         ", *item.Key)
			fmt.Println("Last modified:", *item.LastModified)
			fmt.Println("Size:         ", *item.Size)
			fmt.Println("")
		}
	}
	return counter
}

func getSession(profile string, region *string) *session.Session {
	return session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region: region,
		},
		Profile: profile,
	}))
}

func createS3Service(session *session.Session) *s3.S3 {
	return s3.New(session)
}

func getObjects(service *s3.S3, bucket *string) *s3.ListObjectsOutput {
	s3ListObjects, error := service.ListObjects(&s3.ListObjectsInput{
		Bucket: bucket,
	})
	if error != nil {
		exitErrorf("Unable to list items in bucket %q, %v", bucket, error)
	}
	return s3ListObjects
}